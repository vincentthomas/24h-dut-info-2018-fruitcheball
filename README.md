Ce dépôt contient les fichiers développés pour l'épreuve d'IA des "24h des DUT 2018" organisés à l'IUT Nancy-Charlemagne.

Pour effectuer une copie de ce dépôt en local, utiliser la commande clone : 
```
git clone git@bitbucket.org:vincentthomas/24h-dut-info-2018-fruitcheball.git
```

Il est possible de télécharger l'ensemble du dépôt [sous la forme d'un fichier zip](https://bitbucket.org/vincentthomas/24h-dut-info-2018-fruitcheball/downloads/).

# Fichiers de l'épreuve IA 

Vous trouverez les fichiers de l'épreuve IA dans le répertoire [release/2018_fruitcheball](release/2018_fruitcheball) de ce dépôt : 

* l'énoncé du sujet **fruitcheball.pdf**
* l'application serveur **fruitcheball.jar**
* une application client **fruitcheballClientRandom.jar**

Les fichiers de l'épreuve IA et le code exécutable sont téléchargeables directement à partir de l'archive [2018_fruitcheball.zip](https://bitbucket.org/vincentthomas/24h-dut-info-2018-fruitcheball/downloads/2018_fruitcheball.zip).

![Image de la compétition](release/2018_fruitcheball/image_finale.png)


# Compétition

Merci et félicitations à tous les étudiants participants d'avoir travaillé à proposer des clients (dont certains redoutables) pour un jeu difficile. 

La vidéo d'un des matchs de la finale est [disponible sur la chaine youtube de l'événement](https://www.youtube.com/channel/UC_Cv99m5MGBrGi5h0hv5CCg) et opposait les équipes pour les places du podium. A l'issue des matchs de finale, le classement des 4 équipes finalistes a été : 

* 1ère place : **Connec-tic** de l'IUT Robert Schuman – Strasbourg ;
* 2éme place : **Kaleeis Bears** de l'IUT de Calais ;
* 3ème place : **418 I'm a Teapot** de l'IUT Nancy-Charlemagne ;
* 4ème place : **import team_Caribou; 42** de l'IUT d'Orléans.

# Contenu du dépôt

En plus des fichiers utiles pour l'épreuve d'IA, ce dépôt propose :

* les sources java du serveur de jeu et sa documentation dans le répertoire [sources_java](sources_java) ;
* les sources latex du sujet dans le répertoire [sujet_24h_nancy](sujet_24h_nancy/) ;
* les fichiers générés (pdf + jar + pacman) dans le répertoire [release](release).

A noter, que des clients pour les différents jeux (ainsi que les sujets associés et de la documentation complémentaire) existent mais n'ont pas été ajoutés à ce dépôt pour éviter que les étudiants ne disposent de trop de primitives pour résoudre les sujets de ce type. Des scripts ont aussi été écrits pour lancer automatiquement les tournois mais n'ont pas été inclus dans le dépôt.

Si vous souhaitez accéder à certains de ces documents, n'hésitez pas à me contacter à l'adresse [vincent.thomas@loria.fr](mailto://vincent.thomas@loria.fr).

# Ressources

Ces fichiers ont été produits dans le cadre des 24h de l'IUT de 2018 organisés à Nancy. Les classes s'inspirent en partie des classes développées par **Remi Synave** pour les 24h de l'IUT organisées en 2017 à Calais.

La ressources graphiques utilisées proviennent 

* du site [games-icons.net](http://game-icons.net/)
* du site d'icones [icons8](https://icons8.com/) et plus particulièrement [https://icons8.com/icon/set/fruit/all](https://icons8.com/icon/set/fruit/all)

# Contributions et remerciements

Merci aux enseignants qui ont relu le sujet, proposé de nombreuses remarques et testé le serveur (un merci spécial à Vincent Colotte  pour avoir développé le premier client intelligent, à Bernard Mangeol, Philippe Dosch et Yannick Parmentier pour avoir participé au sujet et à l'animation de l'épreuve et à Geoffrey Groff pour nous avoir rejoint pour l'évaluation des tournois).

Merci à Gérôme Canals pour avoir chapeauté l'organisation de l'événement et à tout le personnel impliqué dans l'organisation sans qui l'épreuve n'aurait pas pu être proposée.

