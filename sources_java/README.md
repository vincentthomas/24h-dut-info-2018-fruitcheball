# Documentation des sources

Les classes proposées ont pour objectif de mettre en place facilement un concours de type IA pour les 24h des IUT.

Elles permettent ainsi:

* de définir des jeux simultanés ou séquentiels à partir  d'interfaces fournies ;
* de proposer différents types de lancement pour un même jeu (textuel, graphique) basés sur ces interfaces ;
* et de cacher la programmation des échanges réseaux via socket (dans des classes du package *generic*).

A noter, que des clients pour les différents jeux (ainsi que les sujets associés et de la documentation complémentaire) existent mais n'ont pas été ajoutés à ce dépôt pour éviter que les étudiants ne disposent de trop de primitives pour résoudre les sujets de ce type. Néanmoins, si vous souhaitez accéder à ces documents, n'hésitez pas à me contacter à l'adresse [vincent.thomas@loria.fr](mailto://vincent.thomas@loria.fr).


# Structuration des classes

Les classes sont structurées par package :

* le package *generic* propose toutes les classes et les interfaces utiles permettant de lancer clients et serveurs
* **chaque jeu** possède ensuite son package dédié avec l'organisation suivante.
* Pour le jeu **fruitcheball**, 
	* un sous-package *fruitcheball.jeu* est destiné à contenir le modèle du jeu (ainsi que toutes les manipulations de données) ;
	* un sous-package *fruitcheball.vue* est destiné à contenir toutes les classes utiles à l'affichage graphique ;
	* un sous-package *fruitcheball.options* destiné à définir les options pour ce jeu et la factory permettant de construire le jeu à partir de ces options (les options sont analysées par les classes du package *generic.options*).

## Package generic

Le package *generic* contient l'ensemble des classes utiles pour développer un nouveau problème. Il est constitué de différents sous-package:

* *generic.jeu* définit les interfaces des jeux possibles (séquentiel ou simultané) ainsi que la classe joueur ;
* *generic.serveurJeu* contient toutes la partie réseau et les classes permettant de lancer le serveur (SocketServeur) et des gérer les échanges avec les clients (Clientconnecte) ;
* *generic.lanceurSequentiel* contient les classes pour lancer des jeux séquentiels dans différentes circonstances (console, graphique, avec IA, réseau, ...) ;
* *generic.lanceurSimultanee* contient les classes pour lancer des jeux simultanés dans différentes circonstances.

![Diagramme de classe du package generic](./doc/generic.png)

D'autres packages fournissent des services supplémentaires:

* *generic.log* définit un loggeur avec une méthode statique et des sous-classes pour modifier le flux de sortie (fichier, console, ...) ; 
* *generic.outilVue* fournit des outils pour aider la construction des vues (utilisation de sprites, couleurs par défaut) ; 
* *generic.analyseurOptions* permet de spécifier les options de jeu et d'analyser les arguments passés à la commande ;
* *generic.replay* permet de sauver les frames de jeu sous forme d'images (pour générer ensuite des films via la commande du genre `mencoder mf://*.png -mf fps=4:type=png -ovc lavc -o sortie.avi `).

Enfin le package *client* permet de développer des clients jouant au hasard en transmettant à partir de l'ensemble des actions possibles stockées dans un tableau.

## Package labyrintheSequentiel

Le package *jeux.labyrintheSequentiel* contient un exemple de jeu simple construit à partir des classes fournies.

![Diagramme de classe des classes pour développer un jeu tour par tour dans un labyrinthe](./doc/labySequentiel.png)

## Autres jeux

Le package *jeux.fruitcheball* contient le jeu fruitcheball utilisé pour les 24h IUT 2018 et le package *jeux.pacman* un jeu de pacman destiné aux étudiants de S1 en 2017.

Ces package sont structurés conformément à la description précédente des packages de jeu (souspackages jeu, ia, options, ...).

# Prise en main

Cette partie est destinée à montrer comment produire son propre jeu avec les classes fournies. On conseille de mettre chaque jeu dans un package séparé, sous-package du package *jeux*.

Cette partie cherche à faire un jeu simpliste:

* chaque joueur déplace un personnage dans un environnement ;
* le jeu se fait en tour par tour (jeu séquentiel).
* le jeu se nommera *labyrintheSequentiel* (un autre jeu *labyrintheSimultanee* permet de jouer de manière simultanée)

## Création du modèle

### Principe

Le modèle et les classes associées sont définies dans *jeux.labyrintheSequentiel.jeu*. Le modèle de jeu principal doit hériter de `JeuSequentiel` du package *generic.jeu*.

Les méthodes abstraite à définir sont les méthodes suivantes :

* la méthode `void evoluerDonnees(int id_joueur, String action)`  correspond à l'exécution d'un pas de temps du joueur numéro *idJoueur* ayant exécuté l'action *action* passée en paramètre.
* la méthode `String getStatut(int numJoueur)` a pour objectif de construire la chaine de caractères (trame) qui sera transmise aux joueurs en cours de partie. Cette chaine représente la vision que possède un joueur sur le jeu. C'est à partir de cette chaine, que le joueur sera amené à prendre sa décision.
* la méthode `boolean etreFini()` permet de spécifier quand le jeu doit s'arrêter (plus de temps, mécanismes de fin de jeu, ...).
* la méthode `double[] getScore()` permet de retourner les scores des joueurs à l'issue de la partie sous la forme d'un tableau de double.
* la méthode `JComponent getVueGraphique()` a pour objectif de retourner un composant graphique affichant le contenu du jeu (qui sera intégré dans une JFrame au lancement).

### Exemple jeu Labyrinthe

Le jeu `JeuLabySeq` lui-même étend la classe `JeuSequentiel` et est constitué de n personnage.

```
#!java
/**
 * permet de representer un jeu de deplacement dans une arene avec plusieurs
 * joueurs
 */
public class JeuLabySeq extends JeuSimultane {

	/**
	 * taille du labyrinthe selon X
	 */
	private int tailleX;

	/**
	 * taille du labyrinthe selon Y
	 */
	private int tailleY;

	/**
	 * le nombre d'iterations
	 */
	int nbIterations;

	/**
	 * les personnages du labyrinthe
	 */
	public Personnage[] personnages;
```

Chaque personnage possède sa position en attribut, ainsi qu'une méthode `void seDeplacer(String commande, int tx, int ty)` permettant de déplacer l'individu en fonction de la commande transmise *commande* et de la taille du labyrinthe *tx* *ty*.

Sur cette base, la méthode `evoluerDonnees()` consiste simplement à déplacer le personnage en fonction de la commande entrée par le joueur courant.

```
#!java
protected void evoluerDonnees(int id_joueur, String action) {
		nbIterations++;
		// deplace personnage
		Personnage personnageCourant = this.personnages[id_joueur];
		personnageCourant.seDeplacer(action, this.tailleX, this.tailleY);
	}
```

La méthode `getStatus(int num)` retourne une chaine de caractères décrivant la situation observée par le joueur (dans ce cas tout le labyrinthe, mais on peut imaginer des perceptions partielles). Cette chaine contient : 

* le numéro du joueur courant ;
* la position de chaque personnage ;
* des séparateurs (ici underscore **_**) pour permettre aux clients de parser facilement la chaine.


```
#!java
/**
 * la vision du joueur numJoueur = la position de tous les personnages
 */
public String getStatut(int numJoueur) {
	// generation de la trame
	String trame = "";
	String separator = "_";

	// on affiche son numero de joueur
	trame += "num," + numJoueur;
	trame += separator;

	// on retourne simplement la suite des couples x,y
	for (int i = 0; i < this.personnages.length; i++) {
		Personnage perso = this.personnages[i];
		// ajoute coordonnee
		trame += perso.toString();
		// ajoute separateur si pas en fin
		if (i != this.personnages.length - 1)
			trame += separator;
	}
	return trame;
}
```

La méthode `etreFini()` retourne un boolean valant vrai si et seulement si le jeu arrive à sa fin. Dans le cadre du labyrinthe, le jeu s'arrête après 10 itérations.

```
#!java
	/**
	 * le jeu s'arrete au bout d'un certain temps
	 */
	public boolean etreFini() {
		return (this.nbIterations > 10);
	}
```

Enfin, la méthode `double[] getScore()` doit retourner le score final des joueurs. Dans notre cas, il s'agit d'un score nul pour chaque joueur (à adapter en fonction de l'attribution des points).

```
#!java
/**
 * @return un score de 0 pour chaque joueur
 */
public double[] getScore() {
	int nbJoueurs = this.getNb();
	return new double[nbJoueurs];
}
```

## Création de la vue

### Principe

La vue associée au modèle doit simplement être construite et retournée par la méthode `JComponent getVueGraphique()`. Cela permet de retourner le composant graphique que l'on souhaite (qui sera ensuite intégré dans une JFrame).

* La vue et les classes associées ont vocation à être décrites dans le sous-package *jeux.monJeu.vue*
* La vue implémente `Observer` afin de faire les mises à jour de manière transparente : la classe abstraite `JeuSequentiel` étend `Observable` et les notifications aux vues sont **déjà incluses** dans la méthode `void executerJeu()`. Le lien entre la vue et le modèle via la méthode `addObserver` peut se faire dans la création de la vue au sein de la méthode `JComponent getVueGraphique()`.



Enfin il est possible d'utiliser les classes du package *generic.outilVue* pour disposer de primitives:

* `GestionSprites` permet de chargement d'images (via des ressources - ce qui permet de packager correctement l'application y compris dans un jar), le stockage des sprites ainsi que leur rendu avec un objet `Graphics`.
* `Couleurs` propose différentes méthodes statiques pratiques (dont une méthode d'attribution de couleur par joueur).

### Exemple LabyrintheSequentiel

Dans le cadre du package labyrinthe, il faut dans un premier temps écrire la méthode `JComponent getVueGraphique()` qui provient de la classe `JeuSimultanee`.

Il faut penser à ajouter la vue parmi les observer du jeu.

```
#!java
/**
 * construit et associe une vue (ici implémentant Observer)
 */
public JComponent getVueGraphique() {
	VueLaby vue = new VueLaby(this);
	this.addObserver(vue);
	return (vue);
}
```

La classe `VueLaby` permet simplement d'afficher la vue sur le jeu lorsque le jeu notifie ses vues.

```
#!java
public class VueLaby extends JPanel implements Observer {

	/**
	 * mise a jour observer
	 */
	public void update(Observable o, Object arg) {
		repaint();
	}

	/**
	 * affichage du jeu
	 */
	public void paint(Graphics g) {
		super.paint(g);
		// operations a definir
	}
}
```

## Création d'une IA et de clients

### Principe

La création d'IA se fait simplement en implémentant l'interface `generic.clientJeu.IA`.

* Une IA repose sur la méthode `String decider(String etat, int num)`.
* A chaque pas de temps, l'IA reçoit une chaine de caractère correspondant à l'état du monde (fournie via la méthode `String getStatut(int numJoueur)` du jeu) ainsi que son numéro de joueur.
* A partir de ces informations, l'IA doit retourner une chaine de caractère correspondant à l'action souhaitée.
* Ces différents éléments peuvent être transmis directement (dans le cas de lancement local) ou via le réseau (dans le cas d'utilisation en mode client/serveur avec des sockets).

Une IA particulière est fournie par défaut. Il s'agit de l'IA `generic.clientJeu.IAJoueur`. Cette IA consiste à demander à l'utilisateur de choisir son action. Cela permet au joueur de prendre la main sur l'application en remplacement d'une IA.

### Exemple jeu Labyrinthe

La classe `IALabyrinthe` destinée à représenter une IA basique pour le jeu labyrinthe implémente l'interface `IA`. La méthode `decider` prend en entrée la description du monde et le numéro du joueur correspondant à l'IA. Cette méthode doit retourner l'action choisie par le joueur suite à la réception de la description du monde.

Dans cet exemple simple, cette méthode retourne simplement une chaine de caractère correspondant à une des commandes choisie au hasard.

```
#!java
/**
 * permet de decider d'un action (ici choisie au hasard)
 *
 * @param description
 *            l'etat du monde
 *
 * @return la commande du joueur
 */
public String decider(String description, int num) {
	// actions possibles pour ce jeu
	String[] actions = { "N", "S", "E", "O" };

	// choisit action au hasard
	int hasard = (int) (Math.random() * actions.length);
	return actions[hasard];
}
```

## Options de jeu

Les options de jeu modifiables au lancement via la console sont gérées via une classe `Options` et le package *generic.options*.

Cependant, comme la création du jeu dépend des options sélectionnées (qui elle-même dépendent de la structure du jeu), pour rester générique, le choix a été fait d'utiliser une factory de jeu permettant

* de définir les options associées au jeu et leur donner des valeurs par défaut;
* de construire l'objet `jeu` du serveur en fonction de ces options (éventuellement modifiées par les options passées au lancement du serveur).

### Principe de gestion des options

Cette partie repose sur la classe `Options` destinée à stocker toutes les options de jeu.

* La classe `Options` suit le patron singleton. Elle permet d'accéder à une map contenant l'ensemble des options connues du jeu ainsi que leurs valeurs.
* Tous les jeux possèdent initialement les mêmes options par défaut définies dans *generic.analyseurOptions.Options* (port du serveur, timeout par défaut, ...).
* Cependant, chaque jeu peut avoir ses propres options définies dans une sous classe de `FactoryJeu` qui lui est dédiée.
* Ces options sont modifiables via les arguments passés au lancement de l'application.

### Paramètres en ligne de commande

Une fois que les options sont définies, il est possible d'utiliser de modifier les paramètres définis au lancement du jeu (serveur, ...). C'est la classe `generic.analyseurOptions.AnalyseurOptions` qui se charge d'analyser et de modifier les options globales à partir des arguments passés au programme java.

* Il est possible d'accéder aux options publiques via l'option ``--help``.
* Certaines options sont masquées, pour accéder à la liste complète, utilisez l'option ``--hidden``.


### Factory et création de l'objet jeu du serveur

Comme les options de jeu sont utilisés en paramètre des appels au constructeur du jeu (par exemple le nombre de joueurs, ...), les options et le jeu construits sont encapsulés dans une `FactoryJeu` qui est transmise au serveur qui se charge de mettre d'analyser les options passées en argument, de mettre à jour ces options avant d'appeler le constructeur du jeu. 



Il faut donc implémenter cette factory pour chaque jeu:

* `void initialiserOptions()` a pour objectif de construire les options utilisées pour le jeu. Cela revient à récupérer l'objet static `Options` via `Options.getOptions()` et à définir les options possibles de jeu
* `JeuSimultane getJeu()` a pour objectif de construire le jeu avec les bonnes options.

Le diagramme de séquence suivant décrit la mise à jour des options de jeu et la construction du jeu.

![Diagramme de séquence du lancement du serveur de jeu](./doc/LancementReseau_sans_interne.png)

### Exemple de LabyrintheSequentiel

Pour le labyrinthe, l'initialisation des options consiste juste à déterminer le nombre de joueurs

```
#!java
/**
 * initialise les options possibles du jeu
 */
public void initialiserOptions() {
	Options opt = Options.getOptions();

	// nombre de joueurs
	opt.setOptions("nbJoueurs", 2, "nombre de joueurs");
}
```

La création du jeu consiste simplement à créer un jeu adapté au nombre de joueurs de l'option

```
/**
 * construit un jeu avec les options
 */
public JeuSequentiel getJeu() {
	// recupere le nombre de joueurs passes en ligne de commande (ou par defaut)
	int val = Options.getOptions().getVal("nbJoueurs");
	// retourne le jeu
	return new JeuLabySeq(val);
}
```

Le fait de passer par une factory avec une méthode abstraite `initialiserOptions()` permet d'être sur de ne pas oublier de déclarer les options par défaut de son jeu.


## Lancement du jeu en local

### Principe

Une fois ces différents éléments décrits, il est possible de lancer le jeu dans différents modes  via les classes du package `generic.lanceur`

* `LancerJeuConsole` permet de lancer le jeu en mode console avec une interaction via des commandes entrées par l'utilisateur.
* `LancerJeuGraphique` permet de lancer le jeu en mode graphique avec des commandes entrées par l'utilisateur.
* `LancerJeuGraphiqueIA` permet de lancer le jeu en mode graphique en passant une factoryIA destinées à générer les IA (sauf le contrôle du joueur)

Chacun de ces lanceurs prennent en paramètre

* un objet `FactoryJeu` destiné à initialiser les options et construire un jeu
* un objet `String[] args` correspondant aux arguments du main (pour mettre à jour les options par défaut)

### Exemple jeu Labyrinthe

Pour lancer le jeu en mode graphique en interagissant avec l'utilisateur via la console, il suffit simplement de créer une `FactoryJeu` à passer au constructeur de `LancerJeuGraphique`. Tous les joueurs sont gérés via des demandes utilisateurs faites sur la console.

```
#!java
/**
 * permet de lancer le jeu labyrinthe en mode graphique
 */
public class MainLabyGraphique {

	/**
	 * lance le jeu en mode graphique
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		LancerJeuSeqGraphique lanceur = new LancerJeuSeqGraphique(args, new FactoryLabySeq());
		lanceur.boucleJeu();
	}
}
```

## Lancement du jeu en réseau

### Principe

Deux classes sont fournies pour la partie réseau:

* `generic.lanceurSequentiel` permet de lancer un serveur en mode réseau. Il attend un nombre de clients égal au nombre de joueurs avant de se lancer.
* `client.IAClient.ClientIA` permet de lancer un client qui se connectera à partir d'une IA.

Les échanges sont globalement les suivants

* le serveur se lance et attend les clients;
* le client se connecte et envoie son nom;
* le serveur lui répond avec son numéro de joueur;
* quand tous les clients sont connectés, le jeu commence.

### jeu labyrinthe

Pour le **serveur**, il suffit juste de créer une factory de jeu de type `FactoryLaby`, de la passer en paramètre à un objet de type `LancerJeuReseau` et d'appeler la méthode `lancerJeu()` sur cet objet.

Les options sont crées par défaut, mises à jour en fonction des arguments passés et le jeu est ensuite créée.

```
#!java
public class MainLabyrintheSequentielReseauServeur {

	/**
	 * lance un serveur reseau à partir d'un jeu labyrinthe
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		LancerJeuSeqReseau lanceur = new LancerJeuSeqReseau(args, new FactoryLabySeq());
		lanceur.lancerJeu();
	}
}
```

**ATTENTION** pour que le fichier jar construit avec ant fonctionne il faut que le serveur ait le bon nom et se situe à la racine du jeu. 

# Compilation et création des jars

La compilation et la création des jars se fait automatiquement via ant avec le fichier build `build.xml` fourni. Les fichiers jar sont générés dans le répertoire `build` avec un répertoire par jeu.
Ce fichier utilise une variable `jeu` qui permet de désigner le package de jeu utilisé.
```
<property name="jeu" value="labyrintheSequentiel" />
```

Cette variable sert

* à savoir quel package utiliser pour le client et serveur (qui contient le jeu)
* à connaitre le nom des classes principales des packages (qui doivent respecter la forme `Main${jeu}ReseauServeur` et `Main${jeu}ReseauClient`)
* à savoir où stocker les fichiers jar construits (dans `build\${jeu}\`)

**ATTENTION** pour que les jar disposent du bon manifest, il est nécessaire que les classes de lancement des clients et serveurs sont correctement nommés.

**ATTENTION** pour que le build se lance correctement, il est aussi nécessaire d'avoir un répertoire avec le nom du projet dans le package `images`


## lancement

Le serveur se lance simplement en lancant le jar
```
java -jar labyrintheServeur-20170629.jar
```

Il est possible de connaitre les options de lancement avec
```
java -jar labyrintheServeur-20170629.jar --help
```


## Fonctionnalités complémentaires

Toutes les options disponibles sont visibles avec l'option `--hidden`

**Gestion des logs**

Avec l'option `-log` il est possible de rediriger les logs (classe singleton Log)

* `-log=0` pour afficher les logs dans la console ;
* `-log=1` pour afficher les logs dans un fichier créé au même niveau que le lancement de l'application ;
* `-log=2` pour ne pas afficher de log

**Temps attente**

Pour pouvoir suivre l'affichage, un temps d'attente artificiel (en milliseconde) a été ajouté entre chaque frame. Il est possible de le modifier (voire de l'annuler).
```
java -jar fruitcheball -attente=0
```

**Mode batch**

Pour gérer les scripts de tournoi, l'option batch passée à 1 permet de fermer l'application dés que le jeu est fini (sans avoir à fermer manuellement la JFrame). Il est aussi possible d'annuler le rendu en mettant l'option rendu à 0.
```
java -jar fruitcheball -rendu=0 -batch=1
```

**Sauvegarde du déroulement d'un match**

Avec l'option `-print`, on peut sauver sous forme d'images l'ensemble du déroulé d'un match. Les images sont sauvées dans un répertoire à l'endroit où le serveur est lancé. Il est alors possible de créer un film à partir des images sauvées. Attention, un problème de classe swing (bug de l'application à corriger) fait qu'il n'est actuellement pas possible d'avoir un affichage en simultanée.
```
java -jar fruitcheball -rendu=0 -print=1
```

**Timeout et temps de réponse**

Par defaut, un client qui ne répond pas dans les temps n'est plus sollicité par le serveur (même si ses personnages restent présents sur le plateau). Il est possible de modifier ce comportement avec l'option `-arrettimeout=0`. Il est aussi possible de modifier le temps d'attente de la réponse du client avec l'option `timeout` (en ms).
```
java -jar fruitcheball -timeout=5000 -arrettimeout=0
```

# Diagrammes de séquence

Pour ne pas surcharger les informations, ces diagrammes n'affichent que les appels internes pertinents. Pour plus d'informations, il est possible de regarder les diagrammes du répertoire [doc/avecAppelsInternes](doc/avecAppelsInternes).

## Lancement du serveur de jeu 

Ce diagramme présente la manière dont le serveur de jeu se lance (avec analyse des options et gestion de la boucle de jeu).

![Diagramme de séquence du lancement du serveur de jeu](./doc/diagseqReseauLancement_sans_interne.png)

## Gestion de l'attente des connections des clients

Ce diagramme présente la manière dont le serveur attend la connexion des différents clients. Il crée un tableau d'objets de type `ClientConnecte` qui lui permettront de gérer les échanges avec les clients lors du déroulement du jeu.


![Diagramme de séquence de l'attente des connexions clients](./doc/diagSeqAttente_sans_interne.png)

## Gestion de la boucle de jeu séquentielle

Ce diagramme présente la manière dont le serveur gère les échanges avec les clients. Il utilise pour cela le tableau d'objets de type `ClientConnecte`.


![Diagramme de séquence de la boucle de jeu sequentielle](./doc/diagSeqEchanges_sans_interne.png)

# Jeux implémentés dans ce dépôt

Dans ce dépot, vous trouverez des tutoriels :

* le jeu `labyrintheSequentiel` qui a servi de tutoriel
* le jeu `labyrintheSimultane` qui constitue le meme tutoriel mais pour un jeu en simultanée (et non plus tour à tour - le jeu évolue quand tous les joueurs ont choisi secrètement leurs actions)

Ce dépôt contient aussi

* le jeu `fruitcheball` utilisé pour les 24h des dut info 2018 à Nancy
* le jeu `pacman` utilisé pour une épreuve en IA à l'IUT Nancy-Charlemagne

# Ressources

Ces classes ont été produites dans le cadre des 24h de l'IUT de 2018 à Nancy.

Elles s'inspirent en partie des classes développées par **Remi Synave** pour les 24h de l'IUT organisées en 2017 à Calais.

La plupart des ressources graphiques utilisées proviennent de

* du site [games-icons.net](http://game-icons.net/)
* du site d'icones [icons8](https://icons8.com/)



