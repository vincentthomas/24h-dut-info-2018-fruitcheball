@startuml
participant "lanceur:\nlancerJeuSeqReseau" as lanceur

->lanceur: executerJeu()
activate lanceur


participant "jeu:\nJeuSequentiel" as jeu



loop

note right lanceur #lightgreen: Un tour de jeu - �changes avec joueur (indice num)



lanceur -> jeu: etreFini()
note right : teste si fin de jeu
activate jeu
lanceur <-- jeu:false
deactivate jeu


lanceur -> jeu: getStatus(num)
note right : construit descriptif du joueur num
activate jeu
lanceur <-- jeu:descr
deactivate jeu

participant "serveur:\nServeurReseau" as serveur

lanceur -> serveur: envoyerClient(num,descr)
activate serveur
participant "client[num]:\nClientConnecte" as client_i

serveur-> client_i: envoyerMessage(descr)
activate client_i
serveur<-- client_i:
deactivate client_i

lanceur <-- serveur
deactivate serveur


lanceur -> serveur: demanderClient(num)
activate serveur

serveur -> client_i: recevoirMessage()
activate client_i
serveur <-- client_i: action
deactivate client_i

lanceur <-- serveur:action
deactivate serveur



lanceur -> jeu: executerJeu(num,action)
note right: execute le tour de jeu avec action
activate jeu
lanceur <-- jeu:
deactivate jeu



end loop




loop i
note right lanceur #lightgreen: envoi fin de jeu

activate lanceur

lanceur -> serveur : envoyerClient(i,"FIN")
activate serveur

serveur -> client_i : envoyerMessage("FIN")
activate client_i
serveur <-- client_i
deactivate client_i


lanceur <-- serveur
deactivate serveur



end loop

<-- lanceur:
deactivate lanceur

hide footbox

@enduml
