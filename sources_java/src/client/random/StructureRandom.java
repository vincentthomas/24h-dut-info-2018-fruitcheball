package client.random;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * client par defaut random
 * 
 * @author vthomas
 * 
 */
public class StructureRandom {

	/**
	 * construit le client et le lance à partir des actions possibles
	 */
	public StructureRandom(String[] args, String[] actions) {

		String ip = "127.0.0.1";
		int port = 1337;

		// gestion IP
		if (args.length > 0) {
			if (args[0].equals("--help")) {
				System.out.println("");
				System.out.println("------------------------------------------------------");
				System.out.println("* utilisation : ");
				System.out.println("    - premier parametre optionnel = adresse IP (defaut 127.0.0.1)");
				System.out.println("    - second parametre optionnel = port (defaut 1337)");
				System.out.println("\n* exemple 1 : ");
				System.out.println("    java -jar clientfruit.jar");
				System.out.println("    => se connecte en 127.0.0.1 port 1337");
				System.out.println("\n* exemple 2 : ");
				System.out.println("    java -jar clientfruit.jar 192.168.0.1 1300");
				System.out.println("    => se connecte en 192.168.0.1 port 1300");
				System.out.println("------------------------------------------------------");
				System.exit(0);
			}

			ip = args[0];
		}

		// gestion du port
		if (args.length > 1) {
			port = Integer.parseInt(args[1]);
		}

		// affichage ecran lancement
		if (args.length == 0)
			System.out.println("pas de parametre ==> connect client en local port 1337");
		else
			System.out.println("parametre ip port ==> connect client ip : " + ip + " port : " + port);

		// creation de la socket
		Socket socket = null;
		try {
			socket = new Socket(ip, port);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erreur de connection a la creation de la socket");
			System.exit(0);
		}

		BufferedWriter writer = null;
		BufferedReader reader = null;

		// recupere les flux
		try {
			InputStream input = socket.getInputStream();
			OutputStream output = socket.getOutputStream();
			writer = new BufferedWriter(new OutputStreamWriter(output));
			reader = new BufferedReader(new InputStreamReader(input));
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erreur de recuperation des flux de la socket");
			System.exit(0);
		}

		// envoi nom
		try {
			writer.write("clientRandom\n");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erreur d'envoi du nom");
			System.exit(0);
		}
		System.out.println("> envoi nom de client 'clientRandom'");

		// recoit numero
		String num = "";
		try {
			num = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erreur de reception num equipe");
			System.exit(0);
		}

		int n = Integer.parseInt(num);
		System.out.println("> recoit num equipe : " + n);

		// boucle d'echange
		String ligne = null;
		try {
			ligne = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erreur de reception message serveur");
			System.exit(0);
		}

		// actions possibles
		int it = 0;

		// tant que le jeu est pas fini
		while (!ligne.equals("FIN")) {
			it++;
			System.out.println("\n\n*** iteration " + it + " ***");
			System.out.println("<< recoit <<" + ligne);
			// creation action aleatoire
			String action = actions[(int) (Math.random() * actions.length)];
			action+="\n";
			
			System.out.println(">> envoie (avec \\n en bout) >>" + action);

			try {
				writer.write(action);
				writer.flush();
			} catch (IOException e1) {
				e1.printStackTrace();
				System.out.println("erreur envoie message vers serveur");
				System.exit(0);
			}

			try {
				ligne = reader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Erreur de reception message serveur");
				System.exit(0);
			}
		}

	}

}
