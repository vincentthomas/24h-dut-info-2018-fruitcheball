package generic.lanceurSequentiel;

import generic.analyseurOptions.AnalyseurOptions;
import generic.analyseurOptions.Options;
import generic.jeu.JeuSequentiel;
import generic.jeu.Joueur;
import generic.log.LogSingleton;

/**
 * permet de lancer un jeu sequentiel
 * 
 * @author vthomas
 */

public abstract class LancerJeuSeq {

	/**
	 * le jeu a lancer
	 */
	JeuSequentiel jeu;

	/**
	 * lance mainAbstract avec les bonnes options
	 */
	public LancerJeuSeq(String[] args, FactoryJeuSeq factory) {
		// initialise les options
		factory.initialiserOptions();

		// analyse les options (via analyseur) et mise a jour des options
		AnalyseurOptions analyse = new AnalyseurOptions(Options.getOptions());
		analyse.analyser(args);

		// on construit le jeu souhaite
		jeu = factory.getJeu();
	}

	/**
	 * on fait tourner le jeu tant qu'il n'est pas fini
	 */
	public void bouclerJeu() {
		// numero du joueur en cours
		jeu.setNumJoueurCours(0);

		// le temps ecoule
		int t = 0;

		// tant que le jeu n'est pas fini
		while (!this.jeu.etreFini()) {
			LogSingleton.ecrire(">>> ************** temps : " + t + " joueur : " + jeu.getNumJoueurCours() + " **********************");

			// demande action au joueur en cours
			String action = echangerJoueur(jeu.getNumJoueurCours());

			// on execute le jeu pour le joueur courant
			this.jeu.executerJeu(jeu.getNumJoueurCours(), action);

			// on affiche le jeu
			// System.out.println(this.jeu.getStatut(0));	
			t++;

			// on ajoute une attente eventuelle
			attendre();
		}

		
		// fin du jeu
		LogSingleton.ecrire("***************fin du jeu *******************");
		LogSingleton.ecrire("Score:");
		double[] scores = this.jeu.getScore();
		for (int i = 0; i < this.jeu.getNb(); i++) {
			Joueur j = this.jeu.getJoueurs().get(i);
			// gerer les noms des joueurs
			LogSingleton.ecrire("equipe " + i + "(" + j.getNom() + ") : " + scores[i]);
		}

	}

	/**
	 * permet de rajouter un temps d'attente si besoin
	 */
	protected void attendre() {

	}

	/**
	 * on demande action du joueur
	 * 
	 * @param numJoueurCours
	 *            le numero du joueur en cours
	 * @return la chaine ecrite
	 */
	abstract protected String echangerJoueur(int numJoueurCours);
}
