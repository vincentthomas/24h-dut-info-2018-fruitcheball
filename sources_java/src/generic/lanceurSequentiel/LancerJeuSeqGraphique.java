package generic.lanceurSequentiel;

import java.util.Scanner;

import javax.swing.JFrame;

import generic.lanceurSequentiel.FactoryJeuSeq;

/**
 * permet de lancer un jeu dans une fenetre mais la lecture des commandes se
 * fait toujours au clavier
 */

public class LancerJeuSeqGraphique extends LancerJeuSeq {

	/**
	 * la frame dans laquelle lancer
	 */
	JFrame frame;

	// scanner pour lire clavier
	Scanner sc;

	/**
	 * permet de creer le lanceur de jeu
	 * 
	 * @param j
	 *            jeu a lancer
	 */
	public LancerJeuSeqGraphique(String[] args, FactoryJeuSeq factory) {
		// recupere le jeu
		super(args, factory);

		// creation du scanner
		sc = new Scanner(System.in);

		// creation de la frame
		frame = new JFrame();
		frame.setContentPane(this.jeu.getVueGraphique());
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	protected String echangerJoueur(int numJoueurCours) {
		// affiche le jeu
		System.out.println(this.jeu.getStatut(numJoueurCours));

		// demande au clavier
		System.out.println("entree commande joueur " + numJoueurCours);
		String commande = sc.nextLine();
		// attention ne pas fermer car cela ferme le clavier

		return (commande);
	}

}
