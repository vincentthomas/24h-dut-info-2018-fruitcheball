package generic.log;

/**
 * permet de ne rien logguer de la partie
 * 
 * @author vthomas
 * 
 */
public class LogVide implements Log {

	@Override
	public void ecrireLog(String s) {
		// ne fait rien
	}

}
