package generic.outilVue;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import generic.log.LogSingleton;

/**
 * un gestionnaire de sprites
 * 
 * @author vincent.thomas
 *
 */
public class GestionSprites {

	/**
	 * permet de faire singleton
	 */
	private static GestionSprites gestionSprite = null;

	/**
	 * une image par defaut en cas de probleme de lecture
	 */
	private static Image DEFAULT = null;

	/**
	 * map qui contient les sprites
	 */
	Map<String, Image> sprites;

	/**
	 * le repertoire qui contient les sprites
	 */
	String repertoire="";

	/**
	 * 
	 * construit un gestionnaire de sprite vide
	 */
	private GestionSprites() {
		this.sprites = new HashMap<>();
	}

	/**
	 * singleton, retourne le gestionnaire
	 * 
	 * @return le singleton qui gere les sprites
	 */
	public static GestionSprites getSprites() {
		if (gestionSprite == null)
			gestionSprite = new GestionSprites();
		return gestionSprite;
	}

	/**
	 * ajoute le sprite a la liste des sprites
	 */
	public void ajouterElement(String nomFichier) {
		// construit le lien vers la resosurce
		InputStream is = getClass().getClassLoader().getResourceAsStream(nomFichier);
		if (is == null)
			throw new Error("image inexistante " + nomFichier);
		try {
			Image image = ImageIO.read(is);

			// enregistre l'image
			String nomPur= nomFichier.substring(this.repertoire.length()+1);
			//System.out.println(nomPur);
			String sansExtension = nomPur.split("\\.")[0];

			// si image deja la
			if (this.sprites.containsKey(sansExtension)) {
				throw new Error("probleme image deja chargee "+sansExtension);
			}
			this.sprites.put(sansExtension, image);

		} catch (IOException e) {
			throw new Error("probleme de chargement de sprite");
		}
	}

	/**
	 * positionne un repertoire par defaut
	 * 
	 * @param rep
	 *            le repertoire par defaut
	 */
	public void setRepertoire(String rep) {
		this.repertoire = rep;
	}

	/**
	 * permet de dessiner un sprite donne
	 * 
	 * @param nom
	 *            nom du sprite
	 * @param i
	 *            abscisse
	 * @param j
	 *            ordonnee
	 * @param taille
	 *            taille du sprite
	 * @param g
	 *            graphics dans lequel dessiner
	 * 
	 */
	public void dessinerSprite(String nom, int x, int y, int taille, Graphics g) {
		Image img = null;
		img = chargerSprite(nom);
		g.drawImage(img, x, y, taille, taille, null);
	}
	
	/**
	 * permet de dessiner un sprite donne avec deux tailles
	 * 
	 * @param nom
	 *            nom du sprite
	 * @param i
	 *            abscisse
	 * @param j
	 *            ordonnee
	 * @param taille
	 *            taille du sprite
	 * @param g
	 *            graphics dans lequel dessiner
	 * 
	 */
	public void dessinerSprite(String nom, int x, int y, int tailleX, int tailleY, Graphics g) {
		Image img = null;
		img = chargerSprite(nom);
		g.drawImage(img, x, y, tailleX, tailleY, null);
	}
	
	

	/**
	 * permet de charger un sprite (s'il n'est pas deja charge)
	 * 
	 * @param nom
	 *            le nom du sprite
	 * @return le sprite
	 */
	private Image chargerSprite(String nom) {
		Image img;
		// on regarde si le sprite existe
		if (this.sprites.containsKey(nom)) {
			img = this.sprites.get(nom);
		} else
		// on tente de le charger si le repertoire est pas null
		if (repertoire != null) {
			ajouterElement(this.repertoire + "/" + nom + ".png");
			if (this.sprites.containsKey(nom)) {
				img = this.sprites.get(nom);
			} else {
				// sprite charge n'existe pas
				LogSingleton.ecrire("Sprite manquant dans le repertoire >" + nom);
				img = imageParDefaut();
			}
		} else {
			// sprite inconnu car pas de repertoire
			LogSingleton.ecrire("Sprite manquant, pas de repertoire >" + nom);
			img = imageParDefaut();
		}
		return img;
	}

	/**
	 * permet de dessiner un sprite en couleur remplace blanc par la couleur
	 * 
	 * @param nom
	 *            nom du sprite
	 * @param x
	 *            position
	 * @param y
	 *            position
	 * @param taille
	 *            taille du sprite
	 * @param c
	 *            couleur qui remplace le noire
	 * @param g
	 *            graphics
	 */
	public void dessinerSpriteCouleur(String nom, int x, int y, int taille, Graphics g, Color c) {
		// construit le nom avec la couleur
		String nomCouleur = nom + "_" + c;

		// soit l'image coloree n'existe pas deja
		if (!this.sprites.containsKey(nomCouleur)) {
			LogSingleton.ecrire("chargement " + nomCouleur);

			// charge image et fait une copie
			BufferedImage imgNB = (BufferedImage) chargerSprite(nom);
			BufferedImage coul = new BufferedImage(imgNB.getWidth(), imgNB.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
			Graphics gCopie = coul.getGraphics();
			gCopie.drawImage(imgNB, 0, 0, imgNB.getWidth(), imgNB.getHeight(),  null);
			gCopie.dispose();

			// on la colorie
			int rgb = c.getRGB();
			for (int i = 0; i < imgNB.getWidth(null); i++)
				for (int j = 0; j < imgNB.getHeight(null); j++) {
					int col = imgNB.getRGB(i, j);
					int red = (col >> 16) & 0xff;
			        int green = (col >> 8) & 0xff;
			        int blue = (col >> 0) & 0xff;
					int alpha = (col >> 24) & 0xff;
					// si c'est noir et non transparent, on colorie
					if (red==255 && green==255 && blue==255 && alpha==255) {
						coul.setRGB(i, j, rgb);
					}
				}

			// on sauve l'image
			this.sprites.put(nomCouleur, coul);
		}

		// on l'affiche
		Image img = this.sprites.get(nomCouleur);
		g.drawImage(img, x, y, taille, taille, null);
	}

	/**
	 * @return image par defaut en cas d'erreur
	 */
	static Image imageParDefaut() {
		if (DEFAULT == null)
			creerImageDefault();
		return DEFAULT;
	}

	/**
	 * creer une image par defaut
	 */
	private static void creerImageDefault() {
		LogSingleton.ecrire("creation image default");
		BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D g = (Graphics2D) image.getGraphics();
		g.setColor(Color.black);
		g.setStroke(new BasicStroke(10));
		g.drawLine(0, 100, 100, 0);
		g.drawLine(0, 0, 100, 100);
		g.drawOval(25, 25, 50, 50);

		g.dispose();
		DEFAULT = image;
	}

	

}
