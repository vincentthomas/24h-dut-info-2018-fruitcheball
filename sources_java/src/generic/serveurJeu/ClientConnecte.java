package generic.serveurJeu;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import generic.analyseurOptions.Options;
import generic.log.LogSingleton;

/**
 * represente un client connecte au serveur
 */
public class ClientConnecte {

	/**
	 * le temps d'attente a la connexion
	 */
	private int Timeout = 3000;

	/**
	 * parametre de configuration du timeOUT
	 */
	private boolean arretApresTimeOut;

	/**
	 * le numero du client
	 */
	int num;

	/**
	 * le nom du client
	 */
	String nom;

	/**
	 * la socket vers le client
	 */
	Socket socket;

	/**
	 * si un client a deja fait un timeout
	 */
	boolean dejaTimeout = false;

	/**
	 * l'IP du client
	 */
	String IP;
	String hostname;

	/**
	 * gestion des flux entree/sortie
	 */
	BufferedReader entree;
	BufferedWriter sortie;

	/**
	 * creer un client connecte a partir de la socket
	 * 
	 * @param clientSocket
	 *            la socket vers le client
	 * @param id
	 *            le num de client
	 */
	public ClientConnecte(Socket clientSocket, int id) {
		this.num = id;
		this.socket = clientSocket;

		// recupere la valeur de timeout dans les options
		this.Timeout = Options.getOptions().getVal("timeout");
		this.arretApresTimeOut = Options.getOptions().getVal("arrettimeout") == 1;

		// precision du temps de reponse du client
		try {
			this.socket.setSoTimeout(this.Timeout);
		}
		// si probleme de connection
		catch (IOException e) {
			LogSingleton.ecrire("Problème lors de la connexion d'une equipe");
			System.exit(0);
		}

		// recuperation IP
		InetSocketAddress adresse = (InetSocketAddress) socket
				.getRemoteSocketAddress();
		String ip = adresse.toString();
		this.IP = ip.split(":")[0];
		this.hostname = adresse.getHostName();

		// creation des flux entree/sortie
		this.creerFluxES();

		// lecture du nom et envoi du numero
		premierContact();
	}

	/**
	 * echange nom et numero entre le client et le serveur
	 */
	private void premierContact() {
		try {
			this.nom = this.recevoirMessage();
			this.envoyerMessage("" + this.num + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//si aucun nom n'a ete envoye
		if (nom.equals(""))
		{
			LogSingleton.ecrire("***ERROR*** le serveur n'a pas reçu de nom");
			LogSingleton.ecrire("- Pensez a envoyer votre nom d'equipe a la connection");
			LogSingleton.ecrire("- N'oubliez pas le retour chariot");
			LogSingleton.ecrire("- N'oubliez pas de valider l'envoi si vous avez des flux avec buffer");
			System.exit(0);
		}
	}

	/**
	 * peermet de creer les flux d'entree et de sortie
	 */
	private void creerFluxES() {
		// recuperation des flux entree
		try {
			InputStream is = socket.getInputStream();
			this.entree = new BufferedReader(new InputStreamReader(is));
		} catch (IOException e) {
			LogSingleton.ecrire("impossible de recuperer flux entree");
			LogSingleton.ecrire(e.getMessage());
		}

		// creation du flux de sortie
		try {
			OutputStream os = socket.getOutputStream();
			this.sortie = new BufferedWriter(new OutputStreamWriter(os));
		} catch (IOException e) {
			LogSingleton.ecrire("impossible de recuperer flux sortie");
			LogSingleton.ecrire(e.getMessage());
		}
	}

	/**
	 * permet d'afficher le client
	 */
	@Override
	public String toString() {
		String sep = " * ";
		String numClient = "client " + this.num;
		String infoIP = sep + this.hostname + sep + "ip " + this.IP;
		String nom = sep + "nom " + this.nom;
		return numClient + infoIP + nom;
	}

	/**
	 * permet d'envoyer un message au client.
	 * <p>
	 * utilise pour fournir l'etat du jeu
	 * 
	 * @param s
	 *            le message a transmettre
	 * @throws IOException
	 *             probleme dans echange message
	 * 
	 */
	public void envoyerMessage(String s) throws IOException {
		// on ajoute un \n en fin s'il y en a pas
		if (!s.endsWith("\n"))
			s = s + "\n";
		this.sortie.write(s);
		this.sortie.flush();

		String chaine = "-> client" + this.num + " : " + s;
		chaine = chaine.replace("\n", "");
		LogSingleton.ecrire(chaine);
	}

	/**
	 * permet de recevoir un message du client
	 * <p>
	 * utilise lorsque le client veut envoyer ses commandes
	 * 
	 * @return commande du client
	 * 
	 * @throws IOException
	 *             le client ne repond pas
	 */
	public String recevoirMessage() throws IOException {
		// si timeout et qu'on disqualifie
		if (dejaTimeout && arretApresTimeOut) {
			LogSingleton.ecrire("> ***ERROR*** Le client " + nom + "("
					+ this.num + ") n'est plus suivi a cause d'un timeout");
			// deja un timeout
			return ("");
		}

		// si pas encore eu de timeOut ou si on ne disqualifie pas
		else {
			try {
				// tout se passe bien on retourne la ligne
				String readLine = this.entree.readLine();
				LogSingleton.ecrire(" <- client" + this.num + " : " + readLine);
				return readLine;
			} catch (SocketTimeoutException | SocketException e) {
				// probleme de temps de reponse
				LogSingleton.ecrire("> ***ERROR*** Le client " + nom + "("
						+ this.num + ") a mis trop de temps a repondre");
				// note qu'il y a eu un timeOut pour disqualifier le client
				dejaTimeout = true;
				return ("");
			}
		}

	}

	/**
	 * ferme les flux
	 */
	public void close() {
		try {
			this.entree.close();
			this.sortie.close();
		} catch (IOException e) {
			LogSingleton.ecrire("probleme fermeture des flux");
			System.exit(0);
		}
	}

	public String getNom() {
		return this.nom;
	}

}
