package jeux.fruitcheball.jeu;

/**
 * represente un fruit
 * 
 * @author vthomas
 * 
 */
public class Fruit implements Comparable<Fruit> {

	/**
	 * les types de fruits
	 */
	private static String[] TYPES = { "mirabelle", "prune", "cherry",
			"framboise", "spiked-shell" };
	/**
	 * leur dangerosite
	 */
	private static boolean[] DANGER = { false, false, false, false, true, true };

	/**
	 * tableau des fruits
	 */
	public static Fruit[] FRUITS = Fruit.getFruits();

	/**
	 * dangereux
	 */
	boolean danger;

	/**
	 * l'id du type de fruit
	 */
	public int idFruit;

	/**
	 * 
	 * @param type
	 *            le type de fruit
	 * @param mal
	 *            fiat des degats
	 */
	private Fruit(int type, boolean mal) {
		this.idFruit = type;
		this.danger = mal;
	}

	/**
	 * creation des constantes de fruit
	 * 
	 * @return le tableau des fruits connus
	 */
	private static Fruit[] getFruits() {
		Fruit[] fruits = new Fruit[TYPES.length];
		for (int i = 0; i < TYPES.length; i++) {
			fruits[i] = new Fruit(i, DANGER[i]);
		}
		return fruits;
	}

	/**
	 * retourne le type du fruit
	 * 
	 * @return
	 */
	public String getType() {
		return TYPES[this.idFruit];
	}

	/**
	 * @return le nom de l'image
	 */
	public String getNomImage() {
		return "fruits/" + this.getType();
	}

	@Override
	/**
	 * hashcode sur le type
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idFruit;
		return result;
	}

	@Override
	/**
	 * test egalite
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fruit other = (Fruit) obj;
		if (idFruit != other.idFruit)
			return false;
		return true;
	}

	@Override
	public int compareTo(Fruit f) {
		return this.idFruit - f.idFruit;
	}

}
