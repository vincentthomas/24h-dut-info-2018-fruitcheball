package jeux.fruitcheball.jeu;

/**
 * les informations pour la serialisation
 * 
 * @author vthomas
 * 
 */
public class SerialisationJeu {
	
	/**
	 * les separateurs de texte pour la conversion en chaine
	 */
	public static String SEP_TEXT = "_";
}
