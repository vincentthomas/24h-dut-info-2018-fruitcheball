package jeux.fruitcheball.jeu;

import java.util.HashSet;
import java.util.Set;

/**
 * represente une zone ou deposer les fruits une zone de depot est constituee de
 * plusieurs positions
 */
public class ZoneDepot {

	/**
	 * les positions de la zone de depart
	 */
	Set<Position> positionDepot;

	/**
	 * equipe correspondante
	 */
	int idEquipe;

	/**
	 * creer une zone de depot
	 * 
	 * @param id
	 *            id de l'equipe
	 */
	public ZoneDepot(int idEq) {
		this.idEquipe = idEq;

		// creation des positions
		this.positionDepot = new HashSet<Position>();
	}

	/**
	 * ajoute position a la zone de depot
	 * 
	 * @param depot
	 */
	public void add(Position p) {
		this.positionDepot.add(p);
	}

	/**
	 * savoir si zone est de depot
	 */
	public boolean etreZoneDepot(Position p) {
		return this.positionDepot.contains(p);
	}

	/**
	 * @return les cases du depot
	 */
	public Set<Position> getSet() {
		return this.positionDepot;
	}

	public String toText() {
		String descr = "";
		int i=0;
		for (Position p : this.positionDepot) {
			descr += "Z"+i+":"+p+",";
			i++;
		}
		
		return descr.substring(0, descr.length()-1);
	}

}
