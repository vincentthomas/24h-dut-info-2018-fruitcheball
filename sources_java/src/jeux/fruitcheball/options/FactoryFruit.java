package jeux.fruitcheball.options;

import jeux.fruitcheball.jeu.JeuFruitcheball;
import generic.analyseurOptions.Options;
import generic.jeu.JeuSequentiel;
import generic.lanceurSequentiel.FactoryJeuSeq;

public class FactoryFruit extends FactoryJeuSeq {

	/**
	 * les options de jeu
	 */
	private Options opt;

	@Override
	/**
	 * permet d'ajouter des options liees au jeu
	 */
	public void initialiserOptions() {
		opt = Options.getOptions();

		// TODO changer les options par defaut pour la release

		// nombre de joueurs
		opt.setOptions("Jeu", "nbJoueurs", 4, "le nombre de joueurs (entre 1 et 4)");
		// le labyrinth
		opt.setOptions("Jeu", "laby", 0, "le labyrinthe utilise (entre 0 et 8 - cf. sujet)");

		// taillle des cases
		opt.setOptions("Graphique", "taille", 40, "la taille des cases");
	}

	@Override
	/**
	 * retourne le jeu avec le bon nombre de joueurs
	 */
	public JeuSequentiel getJeu() {
		return new JeuFruitcheball(opt.getVal("nbJoueurs"));
	}

}
