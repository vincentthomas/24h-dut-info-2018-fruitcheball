package jeux.fruitcheball.vue;

import generic.outilVue.Couleurs;
import generic.outilVue.GestionSprites;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import jeux.fruitcheball.jeu.Personnage;

/**
 * permet de dessiner un meeple
 * 
 * @author vthomas
 * 
 */
public class DessinMeeple {

	/**
	 * dessine le personnage p en (x,y)
	 * 
	 * @param p
	 *            personnage a dessiner
	 * 
	 * @param x
	 *            coordonnee en pixel
	 * @param y
	 *            coordonnee en pixel
	 * @param taille
	 *            taille du personnage
	 */
	public void dessinerMeeple(Personnage p, int x, int y, int taille,
			Graphics g) {

		// recuperation des informations du meeple
		int nEquipe = p.getIdEquipe();
		int numPj = p.getIdPj();
		Color coul = Couleurs.getCouleur(nEquipe);

		GestionSprites sprites = GestionSprites.getSprites();

		// dessin des personnages
		if (p.getType() == Personnage.PERSO_NORMAL) {
			sprites.dessinerSpriteCouleur("meepleWhite", x, y, taille, g, coul);
			sprites.dessinerSprite("meepleTrans", x, y, taille, g);
			// ajoute le nombre
			Graphics2D g2 = (Graphics2D) g;
			g2.setFont(new Font("Courrier New", Font.PLAIN, taille / 2));
			g2.drawString("" + numPj, x + taille / 3, y + 3 * taille / 4);
		} else {
			sprites.dessinerSpriteCouleur("meeple-kingWhite", x, y, taille, g,
					coul);
			sprites.dessinerSprite("meeple-kingTrans", x, y, taille, g);
		}

	}

}
