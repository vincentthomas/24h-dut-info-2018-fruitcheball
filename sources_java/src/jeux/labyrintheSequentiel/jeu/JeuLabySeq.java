package jeux.labyrintheSequentiel.jeu;

import javax.swing.JComponent;
import generic.jeu.JeuSequentiel;
import jeux.labyrintheSequentiel.vue.VueLaby;

/**
 * un jeu de deplacement de labyrinthe au tour par tour avec plusieurs
 * personnages
 * 
 * @author vincent.thomas@loria.fr
 */

public class JeuLabySeq extends JeuSequentiel {

	/**
	 * taille du labyrinthe selon X
	 */
	private int tailleX;

	/**
	 * taille du labyrinthe selon Y
	 */
	private int tailleY;

	/**
	 * le nombre d'iterations
	 */
	int nbIterations;

	/**
	 * les personnages du labyrinthe
	 */
	public Personnage[] personnages;

	/**
	 * creation du labyrinthe sequentiel
	 * 
	 * @param nbJoueurs
	 *            nombre de joueurs dans le labyrinthe
	 */
	public JeuLabySeq(int nbJoueurs) {
		// creation des joueurs
		super(nbJoueurs);

		// creation du labyrinthe
		this.tailleX = 10;
		this.tailleY = 10;
		this.nbIterations = 0;

		// creation des joueurs et des personnages
		this.personnages = new Personnage[nbJoueurs];
		for (int i = 0; i < nbJoueurs; i++)
			// les personnages sont crees par defaut sur la diagonale
			this.personnages[i] = new Personnage(i, i);
	}

	/**
	 * permet de faire evoluer les personnages en fonction des deplacements (methode
	 * de jeuSequentiel)
	 */
	@Override
	protected void evoluerDonnees(int id_joueur, String action) {
		nbIterations++;
		// deplace personnage
		Personnage personnageCourant = this.personnages[id_joueur];
		personnageCourant.seDeplacer(action, this.tailleX, this.tailleY);
	}

	@Override
	/**
	 * vue du joueur numJoueur correspond a la position de tous les personnages
	 */
	public String getStatut(int numJoueur) {
		// generation de la trame
		String trame = "";
		String separator = "_";

		// on affiche son numero de joueur
		trame += "num," + numJoueur;
		trame += separator;

		// on retourne simplement la suite des couples x,y
		for (int i = 0; i < this.personnages.length; i++) {
			Personnage perso = this.personnages[i];
			// ajoute coordonnee
			trame += perso.toString();
			// ajoute separateur si pas en fin
			if (i != this.personnages.length - 1)
				trame += separator;
		}
		return trame;
	}

	@Override
	/**
	 * le jeu s'arrete au bout d'un certain temps
	 */
	public boolean etreFini() {
		return (this.nbIterations > 10);
	}

	@Override
	/**
	 * @return un score de 0 pour chaque joueur
	 */
	public double[] getScore() {
		int nbJoueurs = this.getNb();
		return new double[nbJoueurs];
	}

	@Override
	/**
	 * construit et associe une vue de type Observer
	 */
	public JComponent getVueGraphique() {
		VueLaby vue = new VueLaby(this);
		this.addObserver(vue);
		return (vue);
	}

	////////////////////////////////////////////////////
	/////////// getter /////////////////////////////////
	////////////////////////////////////////////////////

	public int getTailleX() {
		return tailleX;
	}

	public int getTailleY() {
		return tailleY;
	}

}
