package jeux.labyrintheSimultane;

import generic.lanceurSimultane.LancerJeuConsole;
import jeux.labyrintheSimultane.options.FactoryLaby;

/**
 * permet de lancer le jeu labyrinthe en mode console
 */
public class MainLabyConsole {

	/**
	 * lance le jeu en mode console
	 * 
	 * @param args
	 *            inutilise pour l'instant
	 */
	public static void main(String[] args) {
		LancerJeuConsole lanceur = new LancerJeuConsole(args, new FactoryLaby()); 
		lanceur.boucleJeu();
	}
}
