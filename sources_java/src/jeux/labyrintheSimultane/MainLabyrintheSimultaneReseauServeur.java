package jeux.labyrintheSimultane;

import generic.lanceurSimultane.LancerJeuReseau;
import jeux.labyrintheSimultane.options.FactoryLaby;

public class MainLabyrintheSimultaneReseauServeur {

	/**
	 * lance un serveur reseau a partir d'un jeu labyrinthe
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		LancerJeuReseau lanceur = new LancerJeuReseau(args, new FactoryLaby());
		lanceur.lancerJeu();
	}
}
