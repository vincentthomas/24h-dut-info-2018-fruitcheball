package jeux.pacman.jeu;

public class Fantome extends Personnage {

	public Fantome(int id, Position p, int nb) {
		super(id, p, nb);
	}

	@Override
	// retourne F pour fantome
	public String getType() {
		return "F";
	}

	@Override
	public void executerAction(char commande, JeuPacman jeu) {
		// trouve nouvelle case
		int[] nouvelle = this.caseProchaine(commande);
		int nx = nouvelle[0];
		int ny = nouvelle[1];

		// si la case est disponible et pas de fantome
		if (jeu.getLaby().etreDisponible(nx, ny)) {
			// si pas de fantome sur la case
			if (jeu.getFantome(nx, ny) == null) {
				// on se deplace
				this.x = nx;
				this.y = ny;
			}
		}

		// si un pacman dessus, on le mange
		Pacman pacman = jeu.getPacman(nx, ny);
		if (pacman != null) {
			rencontrePacman(jeu, pacman);
		}

	}

	/**
	 * gere la rencontre avec un pacman
	 * 
	 * @param jeu
	 *            jeu en cours
	 * @param pacman
	 *            pacman rencontre
	 */
	private void rencontrePacman(JeuPacman jeu, Pacman pacman) {
		// si pacman a une pacgomme active
		if (pacman.etreSousPacgomme()) {
			// pacman mange fantome this
			pacman.mangerFantome(jeu, this);
		} else {
			// fantome mange pacman
			this.mangerPacman(jeu, pacman);
		}
	}

	/**
	 * quand un fantome mange pacman
	 * 
	 * @param jeu
	 * @param pacman
	 */
	void mangerPacman(JeuPacman jeu, Pacman pacman) {
		// pacman est mange == met a jour les stats
		pacman.getStats().etreManger(this.idJoueur);
		this.getStats().manger(pacman.idJoueur);

		// pacman meurt et perd des points
		pacman.mourir(jeu);
		pacman.points -= JeuPacman.POINTS_FANTOME / 2;

		// pacman gagne les points si tue un adversaire
		if (this.idJoueur != pacman.idJoueur) {
			this.points += JeuPacman.POINTS_FANTOME;
		}
	}

	@Override
	public String getTexte() {
		return "F," + this.x + "," + this.y + "," + this.points;
	}

	@Override
	void mourir(JeuPacman jeu) {
		Position depart = jeu.getLaby().getDepartFantome(this.idJoueur);
		Position test = new Position(depart.x, depart.y);

		// on cherche une place vide autour
		double num_test = 1;
		// tant que la case est pas libre
		while (!jeu.etreDisponible(test.x, test.y)) {
			num_test += 0.1;
			int hasardX = (int) (3 * num_test * Math.random()) - (int) num_test;
			int hasardY = (int) (3 * num_test * Math.random()) - (int) num_test;
			// creer une nouvelle case possible
			test = new Position(depart.x + hasardX, depart.x + hasardY);
		}
		// la case est disponible, met pacman
		this.x = test.x;
		this.y = test.y;
	}

}
