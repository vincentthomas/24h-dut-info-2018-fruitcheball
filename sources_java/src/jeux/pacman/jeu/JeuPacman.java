package jeux.pacman.jeu;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;

import generic.analyseurOptions.Options;
import generic.jeu.*;
import jeux.pacman.vue.VuePacman;

/**
 * jeu ou il faut ramasser des objets
 */
public class JeuPacman extends JeuSequentiel {

	public static final int NB_ITERATIONS = 100;

	/**
	 * les points quand on tue un pacman
	 */
	public static final int POINTS_FANTOME = 10;

	/**
	 * separateur de chaine
	 */
	public static String SEP_TEXT = "_";;

	/**
	 * iterations
	 */
	public int nbIt = 0;

	/**
	 * le labyrinthe dans lequel on evolue
	 */
	private Labyrinthe laby;

	/**
	 * les personnages
	 */
	public List<Personnage> personnages;

	/**
	 * constructeur
	 * 
	 * @param nbj
	 *            nombre de joueur
	 */
	public JeuPacman(int nbj) {
		super(nbj);

		// creation du labyrinthe
		LabyrintheFactory factory = new LabyrintheFactory();
		this.laby = factory.getLaby(Options.getOptions().getOption("laby")
				.getValueInt());

		// creation des personnages
		this.personnages = new ArrayList<Personnage>();
		// chaque joueur a deux persos
		for (int id = 0; id < nbj; id++) {
			// pacman
			Position posP = this.laby.getDepartPacman(id);
			this.personnages.add(new Pacman(id, posP, nbj));

			// fantome
			Position posF = this.laby.getDepartFantome(id);
			this.personnages.add(new Fantome(id, posF, nbj));
		}
	}

	@Override
	protected void evoluerDonnees(int id, String commande) {
		// pour le joueur actif

		// commande perso 1
		if (commande.length() > 0) {
			char action1 = commande.charAt(0);
			Personnage pj1 = this.personnages.get(2 * id);
			pj1.executerAction(action1, this);
		}

		// commande perso 2
		if (commande.length() > 1) {
			char action2 = commande.charAt(1);
			Personnage pj2 = this.personnages.get(2 * id + 1);
			pj2.executerAction(action2, this);
		}
	}

	@Override
	/**
	 * vue du jeu de pacman
	 */
	public JComponent getVueGraphique() {
		VuePacman vue = new VuePacman(this);
		this.addObserver(vue);
		return vue;
	}

	@Override
	// retourne le descriptif du jeu
	public String getStatut(int numJoueur) {
		String etat = "" + numJoueur + SEP_TEXT;

		// retourne d'abord le labyrinthe (taille puis contenu)
		etat += this.laby.toText();

		// ensuite pour chaque joueur
		for (int i = 0; i < this.getNb(); i++) {
			etat += i;
			etat += "#";
			// personnage P
			etat += personnages.get(2 * i).getTexte();
			etat += "#";
			etat += personnages.get(2 * i + 1).getTexte();
			etat += SEP_TEXT;
		}

		return (etat);
	}

	@Override
	public boolean etreFini() {
		this.nbIt++;
		return (this.nbIt > NB_ITERATIONS * this.getNb());
	}

	@Override
	public double[] getScore() {
		double[] score = new double[this.getJoueurs().size()];
		// pour chaque joueur
		for (int i = 0; i < this.getJoueurs().size(); i++) {
			// somme des personnages
			score[i] += this.personnages.get(2 * i).getPoints();
			score[i] += this.personnages.get(2 * i + 1).getPoints();
		}
		return score;
	}

	/**
	 * @return taille X du labyrinthe
	 */
	public int getTailleX() {
		return this.laby.getTailleX();
	}

	/**
	 * @return taille Y
	 */
	public int getTailleY() {
		return this.laby.getTailleY();
	}

	/**
	 * @return labyrinthe
	 */
	public Labyrinthe getLaby() {
		return laby;
	}

	/**
	 * cherche s'il y a des pacman sur la case donnee
	 * 
	 * @param x
	 *            case a chercher
	 * @param y
	 *            case a chercher
	 * @return le pacman present ou null si n'y a pas de pacman
	 */
	public Pacman getPacman(int nx, int ny) {
		// pour chaque personnage
		for (Personnage p : this.personnages) {
			// s'il est a la bonne position
			if (p.etrePosition(nx, ny)) {
				// si c'est un pacman
				if (p.getType().equals("P"))
					return (Pacman) p;
			}
		}
		return null;
	}

	/**
	 * retourne les fantomes de la case
	 * 
	 * @param x
	 *            case a chercher
	 * @param y
	 *            case a chercher
	 * @return les fantomes presents sur la case (ou liste vide)
	 */
	public Fantome getFantome(int nx, int ny) {
		// pour chaque personnage
		for (Personnage p : this.personnages) {
			// s'il est a la bonne position
			if (p.etrePosition(nx, ny)) {
				// si c'est un fantome
				if (p.getType().equals("F"))
					return (Fantome) p;
			}
		}
		return null;
	}

	/**
	 * permet de savoir si la case ne contient personne
	 * 
	 * @param nx
	 *            x de la case teste
	 * @param ny
	 *            y de la case teste
	 * @return
	 */
	public boolean etreSansPersonne(int nx, int ny) {
		return (this.getFantome(nx, ny) == null && this.getPacman(nx, ny) == null);
	}
	
	/**
	 * permet de savoir si une case est disponible
	 * <li> c'est dans le laby
	 * <li> c'est pas un mur 
	 * <il> il n'y a personne
	 * @param nx position x testee
	 * @param ny position y testee
	 * @return true si la case est dispo
	 */
	public boolean etreDisponible(int nx, int ny) {
		// si elle est en dehors ou un mur
		if (!this.laby.etreDisponible(nx, ny))
			return false;
		// sinon disponible si sans personne
		return this.etreSansPersonne(nx, ny);
	}

}
