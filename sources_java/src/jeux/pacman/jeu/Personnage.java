package jeux.pacman.jeu;

/**
 * un personnage
 * 
 * @author vthomas
 * 
 */
public abstract class Personnage {

	/**
	 * l'id du joueur qui controle le personnage
	 */
	int idJoueur;

	/**
	 * coordonnees
	 */
	int x, y;

	/**
	 * l'attribut point
	 */
	protected int points;

	/**
	 * les statistiques
	 */
	private StatManger stats;

	/**
	 * la derniere action faite
	 */
	public char lastAction = 'E';

	/**
	 * constructeur de personnage
	 * 
	 * @param id
	 *            id du joueur qui controle le personnage
	 * @param p
	 *            position initiale
	 * @param nb
	 *            nombre de joueurs
	 */
	public Personnage(int id, Position p, int nb) {
		this.idJoueur = id;
		this.points = 0;

		// initialisation de la position
		this.x = p.x;
		this.y = p.y;

		// initialisation des statistiques
		this.setStats(new StatManger(nb));
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	/**
	 * calcul nextcase
	 */
	public int[] caseProchaine(char commande) {
		// calcul de la nouvelle position
		int nx = this.x;
		int ny = this.y;

		// met a jour derniere action
		char sauve = this.lastAction;
		this.lastAction = commande;

		// en fonction de la commande
		switch (commande) {

		// deplacement nord
		case 'N':
			ny--;
			break;

		// deplacement sud
		case 'S':
			ny++;
			break;

		// deplacement est
		case 'E':
			nx++;
			break;

		// deplacement ouest
		case 'O':
			nx--;
			break;

		// si aucune action connue
		default:
			// pas de commande, on reprend derniere actions
			this.lastAction = sauve;
			break;
		}

		// la nouvelle case
		int[] nouvelle = { nx, ny };
		return nouvelle;

	}

	/**
	 * @return les points du personnage
	 */
	public double getPoints() {
		return this.points;
	}

	/**
	 * retourne id du possesseur
	 * 
	 * @return id du possesseur
	 */
	public int getId() {
		return this.idJoueur;
	}

	/***************************************************************
	 ***************************************************************
	 ***************************************************************
	 */

	/**
	 * @return retourne son type
	 */
	public abstract String getType();

	/**
	 * permet d'executer une action
	 * 
	 * @param commande
	 *            la commande
	 * @param laby
	 *            le labyrinthe
	 */
	public abstract void executerAction(char commande, JeuPacman jeu);

	/**
	 * verifie si un personnage est a la position
	 * 
	 * @param nx
	 *            position x
	 * @param ny
	 *            position en y
	 * @return true si meme position
	 */
	public boolean etrePosition(int nx, int ny) {
		return (this.x == nx && this.y == ny);
	}

	/**
	 * quand un personnage meurt, revient au debut
	 */
	abstract void mourir(JeuPacman jeu);

	/**
	 * savoir si le pj est sous pacgomme
	 * 
	 * @return
	 */
	public boolean etreSousPacgomme() {
		return false;
	}

	/**
	 * permet de modifier la position d'un personnage
	 * 
	 * @param i
	 *            position en i
	 * @param j
	 *            position en j
	 * @param jeu
	 *            labyrinthe
	 */
	public void setPosition(int i, int j, JeuPacman jeu) {
		// si pas de mur
		if (jeu.getLaby().etreDisponible(i, j)) {
			this.x = i;
			this.y = j;
		} else {
			throw new Error("position sur un mur " + i + "," + j);
		}
	}

	public abstract String getTexte();

	public StatManger getStats() {
		return stats;
	}

	public void setStats(StatManger stats) {
		this.stats = stats;
	}
}
