package jeux.pacman.jeu;

/**
 * permet de stocker le nombre de fois ou on a mange et ete mange
 * 
 * @author vthomas
 * 
 */
public class StatManger {

	/**
	 * nombre de fois ou on a ete mange
	 */
	int nbEtreMange[];

	/**
	 * nombre de fois ou on a mange
	 */
	int nbManger[];

	/**
	 * creation des statistiques
	 * 
	 * @param nb
	 *            nombre de joueurs
	 */
	public StatManger(int nb) {
		this.nbEtreMange = new int[nb];
		this.nbManger = new int[nb];
	}

	/**
	 * ajoute le fait d'avoir mange
	 * 
	 * @param id
	 *            id du joueur mange
	 */
	public void manger(int id) {
		this.nbManger[id]++;
	}

	/**
	 * ajoute le fait d'avoir ete mange
	 * 
	 * @param id
	 *            id du joueur mange
	 */
	public void etreManger(int id) {
		this.nbEtreMange[id]++;
	}

	/**
	 * permet d'afficher les stats
	 */
	public String toString() {
		String res = "";
		for (int i : this.nbManger) {
			res += i + " ";
		}
		res+=" - ";
		for (int i : this.nbEtreMange) {
			res += i + " ";
		}
		return res;
	}

}
