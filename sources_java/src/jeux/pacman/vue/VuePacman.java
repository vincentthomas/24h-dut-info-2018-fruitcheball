package jeux.pacman.vue;

// import classes de jeu
import generic.analyseurOptions.Options;
import generic.log.LogSingleton;
import generic.outilVue.Couleurs;
import generic.outilVue.GestionSprites;
import jeux.pacman.jeu.*;

// graphique
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class VuePacman extends JPanel implements Observer {

	private static final int TAILLE_BULLET = 6;
	private static final int TAILLE_PACGOMME = 10;

	/**
	 * la taille des cases
	 */
	private static final int TAILLE = Options.getOptions().getVal("taille");

	/**
	 * gestionaire de sprites
	 */
	GestionSprites sprites;

	/**
	 * le jeu Laby a afficher
	 */
	JeuPacman jeu;

	/**
	 * image de fond
	 */
	BufferedImage fond;

	/**
	 * constructeur de vue laby
	 * 
	 * @param l
	 *            le jeu a afficher
	 */
	public VuePacman(JeuPacman l) {
		this.jeu = l;

		// dimension de la vue
		int dimX = (l.getTailleX() + 9) * TAILLE;
		int dimY = l.getTailleY() * TAILLE;

		// si dimy n'inclut pas les joueurs
		if (dimY < 3 * l.getNb() * TAILLE + 4 * TAILLE) {
			dimY = (3 * l.getNb() * TAILLE) + 4 * TAILLE;
		}

		Dimension size = new Dimension(dimX, dimY);
		this.setPreferredSize(size);
		this.setSize(size);

		// charge fruits
		LogSingleton.ecrire("chargement images");
		this.sprites = GestionSprites.getSprites();
		this.sprites.setRepertoire("images/pacman");

		// creation image statique de fond
		creationFond();
	}

	/**
	 * creation de l'image de fond
	 */
	private void creationFond() {
		// dimension de la vue
		int dimX = this.jeu.getTailleX() * TAILLE;
		int dimY = this.jeu.getTailleY() * TAILLE;

		// creation de l'image de fond
		fond = new BufferedImage(dimX, dimY, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D g = (Graphics2D) fond.getGraphics();
		g.setColor(Color.lightGray);
		g.fillRect(0, 0, dimX, dimY);

		// taille
		int tx = this.jeu.getTailleX();
		int ty = this.jeu.getTailleY();
		g.setColor(Color.BLACK);
		// dessine les cases
		for (int i = 0; i < tx; i++)
			for (int j = 0; j < ty; j++) {
				g.drawRect(i * TAILLE, j * TAILLE, TAILLE, TAILLE);
			}

		// dessin des murs horizontaux
		{
			g.setColor(Color.blue);
			g.setStroke(new BasicStroke(TAILLE, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
			int debutX = -1;
			for (int j = 0; j < ty; j++) {
				for (int i = 0; i < tx; i++) {
					// s'il y a un mur et que ca debute
					if (!this.jeu.getLaby().etreDisponible(i, j)) {
						if (debutX == -1)
							debutX = i;
					}
					// s'il y a pas de mur

					else {
						if (debutX != -1) {
							// on fait un trait entre debutX et ici -1
							g.drawLine(debutX * TAILLE + TAILLE / 2, j * TAILLE + TAILLE / 2, (i) * TAILLE - TAILLE / 2,
									j * TAILLE + TAILLE / 2);
							// on revient au debut
							debutX = -1;
						}
					}
				}
				if (debutX != -1) {
					// on fait un trait entre debutX et ici -1
					g.drawLine(debutX * TAILLE + TAILLE / 2, j * TAILLE + TAILLE / 2, (tx) * TAILLE,
							j * TAILLE + TAILLE / 2);
					// on revient au debut
					debutX = -1;
				}
			}
		}

		// Mur verticaux
		{
			int debutY = -1;
			for (int i = 0; i < tx; i++) {
				for (int j = 0; j < ty; j++) {
					// s'il y a un mur et que ca debute
					if (!this.jeu.getLaby().etreDisponible(i, j)) {
						if (debutY == -1)
							debutY = j;
					}

					// s'il y a pas de mur
					else {
						if (debutY != -1) {
							// on fait un trait entre debutY et ici -1
							g.drawLine(i * TAILLE + TAILLE / 2, debutY * TAILLE + TAILLE / 2, (i) * TAILLE + TAILLE / 2,
									j * TAILLE - TAILLE / 2);
							// on revient au debut
							debutY = -1;
						}
					}
				}
				if (debutY != -1) {
					// on fait un trait entre debutX et ici -1
					g.drawLine(i * TAILLE + TAILLE / 2, debutY * TAILLE + TAILLE / 2, i * TAILLE + TAILLE / 2,
							(ty) * TAILLE);
					// on revient au debut
					debutY = -1;
				}
			}

		}

		g.dispose();
	}

	/**
	 * affichage du jeu
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		// dessin des murs
		g.drawImage(this.fond, 0, 0, null);

		// dessin des fuits
		int tx = this.jeu.getTailleX();
		int ty = this.jeu.getTailleY();
		for (int i = 0; i < tx; i++)
			for (int j = 0; j < ty; j++) {
				Fruit fruit = this.jeu.getLaby().getFruits()[i][j];
				if (fruit != null) {
					if (!fruit.pacgomme()) {
						dessinBullet(g, i, j);
					}
					if (fruit.pacgomme()) {
						dessinPacGomme(g, i, j);
					}
				}
			}

		// dessin des joueurs
		for (Personnage pj : this.jeu.personnages) {
			dessinPersonnage(g, pj);
		}

		// dessin du titre
		this.sprites.dessinerSprite("title", (tx + 1) * TAILLE, TAILLE / 2, 8 * TAILLE, 2 * TAILLE, g);
		// dessin nb iteration
		Graphics2D g2 = (Graphics2D) g;
		g.setColor(Color.black);
		g2.setFont(new Font("Courrier", Font.PLAIN, TAILLE / 2));
		int it = this.jeu.nbIt;
		int total = this.jeu.NB_ITERATIONS * this.jeu.getNb();
		g2.drawString("Iteration : " + it + " / " + total, (tx + 3) * TAILLE, 3 * TAILLE);

		// dessin du score pour chaque joueur
		for (int i = 0; i < this.jeu.getNb(); i++) {
			int ox = (tx + 1) * TAILLE;
			int oy = (3 * i * TAILLE) + 3 * TAILLE;
			score(g, ox, oy, i);
		}

		// cadre autour du joueur en cours
		int num = this.jeu.getNumJoueurCours();
		int cx = (tx + 1) * TAILLE - TAILLE / 2;
		int cy = (3 * num * TAILLE) + 3 * TAILLE + TAILLE / 2;
		g.setColor(Couleurs.getCouleur(num));
		g.drawRect(cx, cy, 8 * TAILLE, 3 * TAILLE);
		g.drawRect(cx + 2, cy + 2, 8 * TAILLE - 4, 3 * TAILLE - 4);
		g.drawRect(cx + 3, cy + 3, 8 * TAILLE - 6, 3 * TAILLE - 6);
		g.setColor(Color.BLACK);
		g.drawRect(cx + 1, cy + 1, 8 * TAILLE - 2, 3 * TAILLE - 2);

	}

	/**
	 * permet de dessiner le score
	 * 
	 * @param g
	 *            graphics dans lequel dessiner
	 * @param ox
	 *            offset en X sur le graphics
	 * @param oy
	 *            offset en Y sur le graphics
	 * @param num
	 *            numero du joueur
	 */
	private void score(Graphics g, int ox, int oy, int num) {
		Color couleurId = Couleurs.getCouleur(num);
		Graphics2D g2 = (Graphics2D) g;

		// dessin nom
		g2.setFont(new Font("Courrier", Font.PLAIN, TAILLE / 2));
		g.drawString(this.jeu.getJoueurs().get(num).getNom(), ox, oy + TAILLE);
		oy = oy + TAILLE / 4;

		// dessin des points pacman
		String nomPj = "pacman" + "_" + 'E';
		this.sprites.dessinerSpriteCouleur(nomPj, ox, oy + TAILLE, TAILLE, g, couleurId);
		g.setColor(Color.BLACK);
		// calcul des points
		String points = " ";
		Personnage pacman = this.jeu.personnages.get(2 * num);
		int points3 = (int) pacman.getPoints();
		if (points3 >= 0)
			points += " ";
		points += points3;
		g2.setFont(new Font("Courrier", Font.PLAIN, 3 * TAILLE / 4));
		g.drawString(points, ox + TAILLE, oy + 2 * TAILLE);
		// affiche les manges
		StatManger manger = pacman.getStats();
		g2.setFont(new Font("Courrier", Font.PLAIN, TAILLE / 2));
		g.drawString(manger.toString(), ox + 3 * TAILLE, oy + 2 * TAILLE);
		g.drawString("    (+)          (-)", ox + 3 * TAILLE, oy + 1 * TAILLE + TAILLE / 4);

		// fantome
		nomPj = "fantome";
		this.sprites.dessinerSpriteCouleur(nomPj, ox, oy + 2 * TAILLE, TAILLE, g, couleurId);
		g.setColor(Color.BLACK);
		Personnage fantome = this.jeu.personnages.get(2 * num + 1);
		int points2 = (int) fantome.getPoints();
		points = "";
		if (points2 >= 0)
			points += " ";
		points += " " + points2;
		g2.setFont(new Font("Courrier", Font.PLAIN, 3 * TAILLE / 4));
		g.drawString(points, ox + TAILLE, oy + 3 * TAILLE);
		// affiche les manges
		StatManger manger2 = fantome.getStats();
		g2.setFont(new Font("Courrier", Font.PLAIN, TAILLE / 2));
		g.drawString(manger2.toString(), ox + 3 * TAILLE, oy + 3 * TAILLE);

	}

	/**
	 * dessin des personnages
	 * 
	 * @param g
	 *            graphics
	 * @param pj
	 *            personnage a dessiner
	 */
	private void dessinPersonnage(Graphics g, Personnage pj) {

		// si pacman
		if (pj.getType().equals("P")) {
			dessinPacman(g, pj);
		}

		// si est fantome
		if (pj.getType().equals("F")) {
			dessinFantome(g, pj);
		}

	}

	/**
	 * permet de dessiner pacman
	 * 
	 * @param g
	 *            grpahics dans lequel dessiner
	 * @param pj
	 *            personnage a dessiner
	 */
	private void dessinPacman(Graphics g, Personnage pj) {
		int x = pj.getX();
		int y = pj.getY();
		Color couleurId = Couleurs.getCouleur(pj.getId());
		// si sous pacgomme
		if (pj.etreSousPacgomme()) {
			String nomPj = "pacman_sup";
			// on ajoute derniere direction
			nomPj = nomPj + "_" + pj.lastAction;
			this.sprites.dessinerSpriteCouleur(nomPj, x * TAILLE, y * TAILLE, TAILLE, g, couleurId);
		} else {
			String nomPj = "pacman";
			// on ajoute derniere direction
			nomPj = nomPj + "_" + pj.lastAction;
			this.sprites.dessinerSpriteCouleur(nomPj, x * TAILLE, y * TAILLE, TAILLE, g, couleurId);
		}
	}

	/**
	 * dessiner un rond pour des icones
	 * 
	 * @param g
	 * @param x
	 * @param y
	 * @param couleurId
	 */
	private void dessinRond(Graphics g, int x, int y, Color couleurId) {
		// dessin fond
		g.setColor(Color.BLACK);
		g.fillOval(x * TAILLE, y * TAILLE, TAILLE, TAILLE);
		g.setColor(couleurId);
		g.fillOval(x * TAILLE + 2, y * TAILLE + 2, TAILLE - 4, TAILLE - 4);
		g.setColor(Color.WHITE);
		g.fillOval(x * TAILLE + 6, y * TAILLE + 6, TAILLE - 12, TAILLE - 12);
		String nomPJ = "ghost";
		// recuperation couleur du joueur
		this.sprites.dessinerSprite(nomPJ, x * TAILLE + TAILLE / 8, y * TAILLE + TAILLE / 8, TAILLE - TAILLE / 4, g);
	}

	/**
	 * permet de dessiner un fantome
	 * 
	 * @param g
	 *            le graphics
	 * @param fantome
	 *            le fantome
	 */
	private void dessinFantome(Graphics g, Personnage fantome) {
		int x = fantome.getX();
		int y = fantome.getY();
		Color couleurId = Couleurs.getCouleur(fantome.getId());
		String nomPJ;
		nomPJ = "fantome";
		this.sprites.dessinerSpriteCouleur(nomPJ, x * TAILLE, y * TAILLE, TAILLE, g, couleurId);
	}

	/**
	 * dessin des pacgomme
	 * 
	 * @param g
	 *            graphics
	 * @param i
	 *            position en i
	 * @param j
	 *            position en j
	 */
	private void dessinPacGomme(Graphics g, int i, int j) {
		int x = i * TAILLE;
		int y = j * TAILLE;
		g.setColor(Color.black);
		g.fillOval(x + TAILLE / 2 - TAILLE_PACGOMME - 2, y + TAILLE / 2 - TAILLE_PACGOMME - 2, 2 * TAILLE_PACGOMME + 4,
				2 * TAILLE_PACGOMME + 4);

		g.setColor(Color.cyan);
		g.fillOval(x + TAILLE / 2 - TAILLE_PACGOMME, y + TAILLE / 2 - TAILLE_PACGOMME, 2 * TAILLE_PACGOMME,
				2 * TAILLE_PACGOMME);
	}

	/**
	 * dessin des bullet
	 * 
	 * @param g
	 *            graphics
	 * @param i
	 *            position en i
	 * @param j
	 *            position en j
	 */
	private void dessinBullet(Graphics g, int i, int j) {
		int x = i * TAILLE;
		int y = j * TAILLE;
		g.setColor(Color.black);
		g.fillOval(x + TAILLE / 2 - TAILLE_BULLET - 2, y + TAILLE / 2 - TAILLE_BULLET - 2, 2 * TAILLE_BULLET + 4,
				2 * TAILLE_BULLET + 4);

		g.setColor(Color.yellow);
		g.fillOval(x + TAILLE / 2 - TAILLE_BULLET, y + TAILLE / 2 - TAILLE_BULLET, 2 * TAILLE_BULLET,
				2 * TAILLE_BULLET);
	}

	/**
	 * mise a jour observer
	 */
	@Override
	public void update(Observable o, Object arg) {
		repaint();
	}

}
